package com.siemens.hackathon.mindhack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Demo1Application {

	public static void main(String[] args) {
		SpringApplication.run(Demo1Application.class, args);
	}
	@RequestMapping(value="/jobs", method=RequestMethod.GET)
	public String test() throws IOException{
		File file = new File("C:\\Users\\m1nmmt\\Documents\\workspace-sts-3.8.4.RELEASE\\demo-1\\src\\main\\resources\\jobs.json");
		StringBuilder sCurrentLine = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String tmp;

			while ((tmp = br.readLine()) != null) {
				sCurrentLine.append(tmp);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return sCurrentLine.toString();
	}
	@RequestMapping(value="/job/{id}", method=RequestMethod.GET)
	public String test1() throws IOException{
		File file = new File("C:\\Users\\m1nmmt\\Documents\\workspace-sts-3.8.4.RELEASE\\demo-1\\src\\main\\resources\\jobDetails.json");
		StringBuilder sCurrentLine = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String tmp;

			while ((tmp = br.readLine()) != null) {
				sCurrentLine.append(tmp);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return sCurrentLine.toString();
	}
}
